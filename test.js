var buf = Buffer(5);
const fs = require('fs')

function create_pkt_primary_hdr(apid, data_len) {
    var hdr = Buffer(6);
    pkt_version = 0b000;
    pkt_type = 0b0; // for telecommand
    pkt_sec_hdr = 0b0;
    apid = apid & 0x07FF; //convert to 11 bit
    hdr.writeUInt16BE(pkt_version << 13 | pkt_type << 12 | pkt_sec_hdr << 11 | apid)

    seq_flg = 0b11;
    seq_cnt = 1;
    hdr.writeUInt16BE(seq_flg << 14 | seq_cnt, 2);
    
    data_len = data_len & 0xFFFF;
    hdr.writeUInt16BE(data_len, 4);

    return hdr;
}

// No of commands
buf.writeUInt8(1)

//1st command 
//Dest address + command type
//dest_addr = 0x24;
//cmd_type = 0b10;
//buf.writeUInt8(dest_addr << 2 | cmd_type, 1)

// Param Length + Command ID
//cmd_id = 0;
//buf.writeUInt8(0b0 << 7 | cmd_id, 2);

//Param for cameraId
//camera_id = 1;
//buf.writeUInt16BE(camera_id, 3);


buf_idx = 1;

//Dest address + command type
dest_addr = 0x25;
cmd_type = 0b10;
buf.writeUInt8(dest_addr << 2 | cmd_type, buf_idx++)

// Param Length + Command ID
cmd_id = 1;
buf.writeUInt8(0b0 << 7 | cmd_id, buf_idx++);

//Param for duration
duration = 10;
angle = -20;

buf.writeUInt8(duration, buf_idx++);
buf.writeInt8(angle, buf_idx++);

/* 2nd command */
//cmd_id = 4;

//buf.writeUInt8(dest_addr << 2 | cmd_type, buf_idx++)
//buf.writeUInt8(0b0 << 7 | cmd_id, buf_idx++);

//exposure = 100;
//buf.writeUInt16BE(exposure, buf_idx);
//buf_idx += 2

hdr = create_pkt_primary_hdr(0x97, 5);

console.log(hdr, buf);

var file = fs.openSync("bin_file", 'w');
fs.writeSync(file, Buffer.concat([hdr, buf]));
fs.closeSync(file)

