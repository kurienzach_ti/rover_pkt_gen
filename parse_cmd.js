const csv = require('csv');
const fs = require('fs');
const xlsx = require('node-xlsx');
const prompt = require('prompt-sync')();
const net = require('net');
const udp = require('dgram');

// Create readable stream from the file
var csvStream = fs.createReadStream('rpcoc_cmd_def.csv');
csvStream.setEncoding('UTF8');

var csv_dict = {
    "act": 1,
    "el": 2,
    "subfield": 3,
    "apid": 10,
    "len": 11,
    "desc": 23,
    "dst_addr": 24,
    "cmd_type": 25,
    "cmd_id": 26,

    // Param related
    "param_name": 4,
    "param_dtype": 13,
    "param_default": 15,
    "param_range": 16,
    "param_max": 29,
    "param_min": 30,
    "param_resolution": 31
}

var cmd_type_map = {
    "Function execute": 0b10,
    "Data Command": 0b11
}

var cmd_dict = [];

var o = {};

/*
 * Function to parse Command CSV
 */
function cmd_csv_parser(err, data) {
    var len = data.length;
    for(let i = 0; i < len; i++) {
        if (i <= 2) continue;
        var line = data[i];

        var cmd = {};

        // Parse the command and cmd properties
        cmd["mnemonic"] = (line[csv_dict["act"]] + " " +
                            line[csv_dict["el"]] + " " +
                            line[csv_dict["subfield"]]).trim()
        cmd["apid"] = Number.parseInt(line[csv_dict["apid"]]);
        cmd["len"] = Number.parseInt(line[csv_dict["len"]]);
        cmd["desc"] = line[csv_dict["desc"]];
        cmd["cmd_type"] = line[csv_dict["cmd_type"]];
        cmd["cmd_id"] = Number.parseInt(line[csv_dict["cmd_id"]]);
        cmd["dst_addr"] = Number.parseInt(line[csv_dict["dst_addr"]]);
        cmd["params"] = []

        i++;
        // Parse the params and their properties
        while(data[i] && data[i][csv_dict["param_name"]] != undefined 
            && data[i][csv_dict["param_name"]] != "") {
            var param = {};
            param["name"] = data[i][csv_dict["param_name"]]
            param["dtype"] = data[i][csv_dict["param_dtype"]]
            // param["map"] = data[i][csv_dict["map"]]
            param["default"] = Number.parseInt(data[i][csv_dict["param_default"]])
            param["range"] = data[i][csv_dict["param_range"]].split(" - ").map(x => Number.parseInt(x));
            param["max"] = Number.parseFloat(data[i][csv_dict["param_max"]])
            param["min"] = Number.parseFloat(data[i][csv_dict["param_min"]])
            param["resolution"] = Number.parseFloat(data[i][csv_dict["param_resolution"]])

            //console.log(param["min"], param["max"], param["resolution"])

            cmd["params"].push(param);
            i++;
        }
        i--;

        cmd_dict.push(cmd);

    }

    //console.log(cmd_dict);
    //console.log( cmd_dict.map(x => x["params"].map((x) => (x["dtype"]))))
    //var new_cmd = {
        //mnemonic: "capture roview still_img",
        //params: {
            //camera_id: 1,
            //request_id: 2,
            //response: 3
        //}
    //}
    //var new_cmd = {
        //mnemonic: "reboot rpcoc_hk",
        //params: {
        //}
    //}

    //console.log(create_RTC_Pkt(new_cmd));

}

function validate_dtype(value, param) {
    //console.log(value, param)

    type = param["dtype"]
    range = param["range"]
    min = param["min"]
    max = param["max"]

    // All values should be numbers
    value = Number.parseFloat(value);
    if (value == NaN) return false;

    // Check if the data type is signed or unsigned
    if ((type[0] === 'U' || type[0] === 'D') && value < 0) return false;

    console.log("Min :"  + min + "Max: " + max + "value : " + value);
    // Check if value is within specified range
    if (value < min || value > max) return false;

    return true;
}

function get_cmd_index(cmd_mnemonic) {
    var len = cmd_dict.length;
    for(var i = 0; i < len; i++) {
        if (cmd_dict[i]["mnemonic"] === cmd_mnemonic) 
            return i;
    }

    return -1;
}

function get_cmd_param_idx(cmd, param_name) {
    var param_len = cmd["params"].length;
    for(var i = 0; i < param_len; i++) {
        if (cmd["params"][i]["name"] === param_name)
            return i;
    }
    return -1;
}

function construct_mask(size) {
    var mask = 0;
    for (var i = 0; i < size; i++) {
        mask = mask << 1 | 0b1;
    }
    return mask;
}

o.create_RTC_Pkt = function (cmd) {
    // Validations
    if (cmd["mnemonic"] == undefined || cmd["params"] == undefined) { 
        console.log("Invalid JSON for command")
        return -1;
    }

    // Check if the CMD mnemonic exists in the dictionary
    var cmd_idx = get_cmd_index(cmd["mnemonic"]);
    if (cmd_idx == -1) { 
        console.log("Invalid command mnemonic")
        return -1;
    }

    var curr_cmd = cmd_dict[cmd_idx];

    // Validate the params
    var params = cmd["params"];
    var param_len = params.length;
    var param_val = 0
    var param_bit_idx = 0;
    var actual_param_len = 0;

    for(param in params) {
        if (!params.hasOwnProperty(param)) continue;

        var param_idx = get_cmd_param_idx(curr_cmd, param)

        // Param doesn't exist in dictionary
        if (param_idx == -1) {
            console.log("Invalid param for command " + param)
            return -1;
        }
        // Validate the param type
            /*if (!validate_dtype(params[param], curr_cmd["params"][param_idx])) {
            console.log("Error while validating the parameter " + param)
            return -1;
        }*/

        //params[param] = Number.parseInt(params[param] / curr_cmd["params"][param_idx]["resolution"])
        var bit_size = Number.parseInt(curr_cmd["params"][param_idx]["dtype"].substr(1));
        actual_param_len += bit_size;
    }

    var total_param_len = curr_cmd["len"] - 16;
    console.log("param len: " + total_param_len);
    console.log("actual param len: " + actual_param_len);
    param_bit_idx = total_param_len - actual_param_len;

    for(param in params) {
        if (!params.hasOwnProperty(param)) continue;

        var param_idx = get_cmd_param_idx(curr_cmd, param)

        // Param doesn't exist in dictionary
        if (param_idx == -1) {
            console.log("Invalid param for command " + param)
            return -1;
        }
        // Validate the param type
        if (!validate_dtype(params[param], curr_cmd["params"][param_idx])) {
            console.log("Error while validating the parameter " + param)
            return -1;
        }

        params[param] = Number.parseInt(params[param] / curr_cmd["params"][param_idx]["resolution"])

        var bit_size = Number.parseInt(curr_cmd["params"][param_idx]["dtype"].substr(1));
        //console.log(bit_size)
        param_bit_idx += bit_size;
        //console.log(param_bit_idx)
        param_val |= (params[param] & construct_mask(bit_size)) <<  (curr_cmd["len"] - 16 - param_bit_idx);
        console.log(params[param], construct_mask(bit_size), curr_cmd["len"] - 16 - param_bit_idx, param_val)
    }

    // Allocate buffer based on length of the command
    var buf;
    if (curr_cmd["len"] == 32)
        buf = Buffer(4);
    else if (curr_cmd["len"] == 48)
        buf = Buffer(6)

    var buf_idx = 0;
    buf.writeUInt8(curr_cmd["dst_addr"] << 2 | cmd_type_map[curr_cmd["cmd_type"]], buf_idx++);

    var param_len = (curr_cmd["len"] == 32)?0b0:0b1;
    console.log("Command ID is "+curr_cmd["cmd_id"])
    buf.writeUInt8(param_len << 7 | curr_cmd["cmd_id"] & 0x7f, buf_idx++);

    // Pack the params
    if (curr_cmd["len"] == 32)
        buf.writeUInt16BE(param_val>>>0, buf_idx);
    else 
        buf.writeUInt32BE(param_val>>>0, buf_idx);

    return buf;
}


o.parseCSV= function() {
    // csvStream.pipe(csv.parse(cmd_csv_parser)).on('finish', function() {
    //     console.log('Completed Parsing of CSV')
    // })

    try {
        cmd_csv_parser(null, xlsx.parse('data.xlsx')[0].data);
    }
    catch (err) {
        console.log("Error in parse XLS file!!")
        console.log(err);
    }
    finally {
        console.log('Parsing complete');
    }
}

function create_pkt_primary_hdr(apid, data_len) {
    var hdr = Buffer(6);
    pkt_version = 0b000;
    pkt_type = 0b1; // for telecommand
    pkt_sec_hdr = 0b0;
    apid = apid & 0x07FF; //convert to 11 bit
    hdr.writeUInt16BE(pkt_version << 13 | pkt_type << 12 | pkt_sec_hdr << 11 | apid)

    seq_flg = 0b11;
    seq_cnt = 1; // seq count is always 1 for testing purpose
    hdr.writeUInt16BE(seq_flg << 14 | seq_cnt, 2);
    
    data_len = data_len & 0xFFFF;
    hdr.writeUInt16BE(data_len, 4);

    return hdr;
}

o.createPacket = function(data, padding) {
    if (padding == null) 
        padding = false

    data_len = data.length;
    cmd_bufs = []
    buf_len = 0;

    for (let i = 0; i < data_len; i++) {
        buf = o.create_RTC_Pkt(data[i]);
        if (buf == -1) {
            return;
        }
        cmd_bufs.push(buf);
        buf_len += buf.length;
    }

    cmd_len = Buffer(1);
    cmd_len.writeUInt8(data_len-1);
    cmd_bufs.unshift(cmd_len);
    cmd_bufs.unshift(create_pkt_primary_hdr(0x40, buf_len));

    //var preface = new Buffer('4,1115\n');
    //preface = JSON.stringify(preface);
    //preface = new Buffer(JSON.parse(preface));
    //cmd_bufs.unshift(preface);

    var file = fs.openSync("bin_file", 'w');
    pkt_data = Buffer.concat(cmd_bufs)
    fs.writeSync(file, pkt_data);

    if (padding) {
        var rem = Buffer(1016  + 7 - pkt_data.length)
        fs.writeSync(file, rem);
    }

    fs.closeSync(file)

    console.log("bin_file created")

    return cmd_bufs
}

o.cmd_queue = [];
o.msgId = 1;

o.cli = function() {
    const MSG = `
1. Parse XLS file
2. Add command
3. Show frame
4. Clear commands
5. Generate bin file
6. Send to command scheduler
7. Exit
    `

    let option = 0;

    while(option != 7) {
        console.log(MSG);
        option = prompt('Enter option : ');
        switch(option) {
            case '1':
                o.parseCSV();
                break;
            case '2':
                let ip_cmd = {};
                ip_cmd['mnemonic'] = prompt('Enter cmd mnemonic : ');

                // Check if the CMD mnemonic exists in the dictionary
                var cmd_idx = get_cmd_index(ip_cmd['mnemonic']);
                if (cmd_idx == -1) { 
                    console.log("Invalid command mnemonic")
                    break;
                }

                // Get parameters
                ip_cmd['params'] = {};
                let curr_cmd = cmd_dict[cmd_idx];
                for(let i = 0; i < curr_cmd.params.length; i++) {
                    ip_cmd['params'][curr_cmd.params[i].name] = 0;
                    let param = prompt('Enter param ' + curr_cmd.params[i].name + ' : ');
                    ip_cmd['params'][curr_cmd.params[i].name] = Number(param);
                    console.log("PARAM = " + Number(param));
                }

                o.cmd_queue.push(JSON.parse(JSON.stringify(ip_cmd)));

                let buf;
                try {
                    buf = o.create_RTC_Pkt(ip_cmd);
                    if (buf == -1)
                        break;
                }
                catch(err) {
                    console.log('Invalid param value');
                    console.log(err);
                    break;
                }

                break;
            case '3':
            console.log(o.cmd_queue);
                break;
            case '4':
                o.cmd_queue = [];
                break;
            case '5':
                o.createPacket(o.cmd_queue, false);
                break;
            case '6':
                let buffer = o.createPacket(o.cmd_queue, false);

                var client = udp.createSocket('udp4');
                var ackServer = udp.createSocket('udp4');

                bufferLen = 0;
                for(let i = 0; i < buffer.length; i++) {
                    bufferLen += buffer[i].length;
                }

                let msgHdr = Buffer(6);
                msgHdr.writeUInt32BE(o.msgId++, 0);
                msgHdr.writeUInt16BE(bufferLen, 4);
                buffer.unshift(msgHdr)

                console.log(buffer);

                ackServer.bind(50100);
                ackServer.on('message', function(data) {
                    console.log(data);
                })

                client.send(buffer, 50000, '10.10.5.126', function(err) {
                    if(err){
                        console.log(err);
                        client.close();
                    } else{
                        console.log('Data sent !!!');
                    }
                });

                client.on('close', function() {
                    console.log('Connection closed');
                });
                break;
        }
    }
}

module.exports = o;
